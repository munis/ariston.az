from modeltranslation.translator import translator, TranslationOptions
from news.models import *


class ProductTranslationOption(TranslationOptions):
    fields = ('title', 'sub_title', 'content')


class CategoryTranslationOption(TranslationOptions):
    fields = ('name',)


translator.register(Product, ProductTranslationOption)
translator.register(Category, CategoryTranslationOption)
