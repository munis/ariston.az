from django.db import models
from time import time
from ckeditor.fields import RichTextField
# Create your models here.

def get_slider_image_path(instance, filename):
    return "slider/%s_%s" % (str(time()).replace('.', '_'), filename)

def get_product_image_path(instance, filename):
    return "products/%s_%s" % (str(time()).replace('.', '_'), filename)




class Slider(models.Model):
    name = models.CharField(max_length=255,null=True,blank=True)
    image = models.ImageField(upload_to=get_slider_image_path)
    text = models.TextField(null=True,blank=True)
    status = models.BooleanField(default=True)

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Slayder'
        verbose_name_plural = 'Slayderlər'

    def __str__(self):
        return "%s" % self.name



class Category(models.Model):
    name = models.CharField(max_length=255)
    status = models.BooleanField(default=True)
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Kateqoriya'
        verbose_name_plural = 'Kateqoriyalar'

    def __str__(self):
        return self.name






class Product(models.Model):
    title = models.CharField(max_length=255)
    sub_title = models.CharField(max_length=255)
    cover_picture = models.ImageField(upload_to=get_product_image_path)
    content = RichTextField(config_name='awesome_ckeditor', null=True, blank=True)
    status = models.BooleanField(default=True)
    date = models.DateTimeField(auto_now_add=True)
    category = models.ForeignKey('Category')

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Məhsul'
        verbose_name_plural = 'Məhsullar'

    def __str__(self):
        return self.title