from django.contrib import admin
from news.models import Slider, Product, Category
# Register your models here.

class ProductAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Başlıq', {
            'fields': ['title_az', 'title_en']
        }),
        ('Sub title', {
            'fields': ['sub_title_az', 'sub_title_en']
        }),
        ('Başlıq', {
            'fields': ['content_az', 'content_en']
        }),
        ('Digeri', {
            'fields': ['cover_picture', 'status','category']
        }),


    ]

class CategoryAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Başlıq', {
            'fields': ['name_az', 'name_en']
        }),
        ('Digeri', {
            'fields': ['status']
        }),


    ]

admin.site.register(Slider)
admin.site.register(Product, ProductAdmin)
admin.site.register(Category, CategoryAdmin)