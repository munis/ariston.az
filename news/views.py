from django.shortcuts import render
from django.views.generic import TemplateView,View
from news.models import Slider, Product
# Create your views here.





class BaseIndexView(TemplateView):
    """
        This is base index view to
    """
    template_name = 'base/index.html'

    def get_context_data(self, **kwargs):
        context = super(BaseIndexView, self).get_context_data(**kwargs)
        context['products'] = Product.objects.all()
        context['slider'] = Slider.objects.filter(status=True)
        return context
