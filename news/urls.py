from django.conf.urls import url
from news.views import BaseIndexView

urlpatterns = [
    url(r'^$', BaseIndexView.as_view(), name="index"),
    # url(r'^', include("news.urls")),
]