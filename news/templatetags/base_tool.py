from django import template

register = template.Library()


@register.filter
def get_image_path(img):
    return '/media/' + str(img)